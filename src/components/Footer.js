import React from 'react';
import FooterNote from './FooterNote';

const Footer = (props) => {
    return(
        <div>
            {props.text}
            <div>
                <FooterNote text="Repetir mais!" feeling="<3" />
            </div>
        </div>
    );
}

export default Footer;
