import React from 'react';
import TitleNote from './TitleNote';
import SubTitleNote from './SubTitleNote';

const Title = (props) => {
    return(
        <div>
            <h1>{props.text}</h1>
            <div>
            <TitleNote text="Repetir mais ainda!" feeling={props.feeling} />                  
            </div>  
            <div>
            <SubTitleNote text="Manda mais repetição" feeling={props.feeling} />
            </div>      
        </div>
        
       
    );
}

export default Title;