import React from 'react';

const FooterNote = (props) => {
    return(
        <div>
            {props.text}{props.feeling}
        </div>
    );

}
export default FooterNote;