import React from 'react';

const SubTitle = (props) => {
    return(
        <h2>
            {props.text}{props.feeling}
        </h2>
    );
}

export default SubTitle;