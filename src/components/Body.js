import React from 'react';
import Note from './Note';

const Body = (props) => {
    return(
        <div>
            {props.text}   
            <div>  
            <Note text="Vou continuar praticando" feeling={props.feeling} />
            </div>
        </div>
    );
}
export default Body;